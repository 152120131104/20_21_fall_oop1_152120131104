#include <iostream>
#include <sstream>
using namespace std;

class Student {
private:
    int yas, std;
    string firstname, lastname;
public:
    void set_age(int age) {
        yas = age;
    }
    int get_age() {
        return yas;
    }

    string get_first_name() {
        return firstname;
    }
    string get_last_name() {
        return lastname;
    }
    void set_first_name(string first_name) {
        firstname = first_name;
    }
    void set_last_name(string last_name) {
        lastname = last_name;
    }
    int get_standard() {
        return std;
    }
    void set_standard(int standard) {
        std = standard;
    }
    string to_string() {
        cout << yas << "," << firstname << "," << lastname << "," << std;
        return " ";
    }
};

int main() {
    int age, standard;
    string first_name, last_name;

    cin >> age >> first_name >> last_name >> standard;

    Student st;
    st.set_age(age);
    st.set_standard(standard);
    st.set_first_name(first_name);
    st.set_last_name(last_name);

    cout << st.get_age() << "\n";
    cout << st.get_last_name() << ", " << st.get_first_name() << "\n";
    cout << st.get_standard() << "\n";
    cout << "\n";
    cout << st.to_string();

    return 0;
}