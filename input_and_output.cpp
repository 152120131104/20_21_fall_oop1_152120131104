include <iostream>
#include <cmath>
#include <cstdio>
#include <algorithm>
#include <vector>
using namespace std;

int main()
{
    int a, b, c, sum;
    cin >> a >> b >> c;
    sum = a + b + c; // sum of three numbers
    cout << sum;
    return 0;
}